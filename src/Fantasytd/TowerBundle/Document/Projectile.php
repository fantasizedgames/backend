<?php

// src/Fantasytd/TowerBundle/Document/Projectile.php
namespace Fantasytd\TowerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\EmbeddedDocument
 */
class Projectile {

  /**
   * @MongoDB\String
   */
  protected $name;

  /**
   * @MongoDB\int
   */
  protected $speed;

  /**
   * @MongoDB\int
   */
  protected $type;

  /**
   * @MongoDB\int
   */
  protected $level;

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\TowerBundle\Document\Effect")
   */
  protected $effects = array();

  /**
   * @MongoDB\String
   */
  protected $sprite;

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\TowerBundle\Document\Animation")
   */
  protected $animations = array();

  /**
   * @MongoDB\EmbedOne(targetDocument="Fantasytd\TowerBundle\Document\Size")
   */
  protected $size;

  public function __construct()
  {
    $this->effects = new \Doctrine\Common\Collections\ArrayCollection();
    $this->animations = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Set name
   *
   * @param string $name
   * @return \Projectile
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Get name
   *
   * @return string $name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set speed
   *
   * @param int $speed
   * @return \Projectile
   */
  public function setSpeed($speed)
  {
    $this->speed = $speed;
    return $this;
  }

  /**
   * Get speed
   *
   * @return int $speed
   */
  public function getSpeed()
  {
    return $this->speed;
  }

  /**
   * Set type
   *
   * @param int $type
   * @return \Projectile
   */
  public function setType($type)
  {
    $this->type = $type;
    return $this;
  }

  /**
   * Get type
   *
   * @return int $type
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set level
   *
   * @param int $level
   * @return \Projectile
   */
  public function setLevel($level)
  {
    $this->level = $level;
    return $this;
  }

  /**
   * Get level
   *
   * @return int $level
   */
  public function getLevel()
  {
    return $this->level;
  }

  /**
   * Add effects
   *
   * @param $effects
   */
  public function addEffect($effects)
  {
    $this->effects[] = $effects;
  }

  /**
   * Remove effects
   *
   * @param <variableType$effects
   */
  public function removeEffect($effects)
  {
    $this->effects->removeElement($effects);
  }

  /**
   * Get effects
   *
   * @return Doctrine\Common\Collections\Collection $effects
   */
  public function getEffects()
  {
    return $this->effects;
  }

  /**
   * Set sprite
   *
   * @param string $sprite
   * @return \Projectile
   */
  public function setSprite($sprite)
  {
    $this->sprite = $sprite;
    return $this;
  }

  /**
   * Get sprite
   *
   * @return string $sprite
   */
  public function getSprite()
  {
    return $this->sprite;
  }

  /**
   * Add animations
   *
   * @param $animations
   */
  public function addAnimation($animations)
  {
    $this->animations[] = $animations;
  }

  /**
   * Remove animations
   *
   * @param <variableType$animations
   */
  public function removeAnimation($animations)
  {
    $this->animations->removeElement($animations);
  }

  /**
   * Get animations
   *
   * @return Doctrine\Common\Collections\Collection $animations
   */
  public function getAnimations()
  {
    return $this->animations;
  }

  /**
   * Set size
   *
   * @param $size
   * @return \Projectile
   */
  public function setSize($size)
  {
    $this->size = $size;
    return $this;
  }

  /**
   * Get size
   *
   * @return $size
   */
  public function getSize()
  {
    return $this->size;
  }
}
