<?php

// src/Fantasytd/TowerBundle/Document/Animation.php
namespace Fantasytd\TowerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\EmbeddedDocument
 */
class Animation {

  /**
   * @MongoDB\String
   */
  protected $name;

  /**
   * @MongoDB\float
   */
  protected $time;

  /**
   * @MongoDB\int
   */
  protected $level;

  /**
   * @MongoDB\Hash
   */
  protected $sequence = array();

  /**
   * Set name
   *
   * @param string $name
   * @return \Animation
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Get name
   *
   * @return string $name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set time
   *
   * @param float $time
   * @return \Animation
   */
  public function setTime($time)
  {
    $this->time = $time;
    return $this;
  }

  /**
   * Get time
   *
   * @return float $time
   */
  public function getTime()
  {
    return $this->time;
  }

  /**
   * Set level
   *
   * @param int $level
   * @return \Animation
   */
  public function setLevel($level)
  {
    $this->level = $level;
    return $this;
  }

  /**
   * Get level
   *
   * @return int $level
   */
  public function getLevel()
  {
    return $this->level;
  }

  /**
   * Set sequence
   *
   * @param hash $sequence
   * @return \Animation
   */
  public function setSequence($sequence)
  {
    $this->sequence = $sequence;
    return $this;
  }

  /**
   * Get sequence
   *
   * @return hash $sequence
   */
  public function getSequence()
  {
    return $this->sequence;
  }
}
