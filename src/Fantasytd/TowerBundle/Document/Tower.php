<?php

// src/Fantasytd/TowerBundle/Document/Tower.php
namespace Fantasytd\TowerBundle\Document;

use Fantasytd\TowerBundle\Document\Size;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Tower {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\String
   */
  protected $name;

  /**
   * @MongoDB\int
   */
  protected $level;

  /**
   * @MongoDB\String
   */
  protected $rarity;

  /**
   * @MongoDB\int
   */
  protected $minDamage;

  /**
   * @MongoDB\int
   */
  protected $maxDamage;

  /**
   * @MongoDB\String
   */
  protected $damageType;

  /**
   * @MongoDB\int
   */
  protected $range;

  /**
   * @MongoDB\float
   */
  protected $speed;

  /**
   * @MongoDB\int
   */
  protected $price;

  /**
   * @MongoDB\int
   */
  protected $upgradePercentage;

  /**
   * @MongoDB\String
   */
  protected $sprite;

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\TowerBundle\Document\Animation")
   */
  protected $animations = array();

  /**
   * @MongoDB\EmbedOne(targetDocument="Fantasytd\TowerBundle\Document\Size")
   */
  protected $size;

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\TowerBundle\Document\Projectile")
   */
  protected $projectiles = array();

  public function __construct()
  {
    $this->animations = new \Doctrine\Common\Collections\ArrayCollection();
    $this->projectiles = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   *
   * @param string $name
   * @return \Tower
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Get name
   *
   * @return string $name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set level
   *
   * @param int $level
   * @return \Tower
   */
  public function setLevel($level)
  {
    $this->level = $level;
    return $this;
  }

  /**
   * Get level
   *
   * @return int $level
   */
  public function getLevel()
  {
    return $this->level;
  }

  /**
   * Set rarity
   *
   * @param string $rarity
   * @return \Tower
   */
  public function setRarity($rarity)
  {
    $this->rarity = $rarity;
    return $this;
  }

  /**
   * Get rarity
   *
   * @return string $rarity
   */
  public function getRarity()
  {
    return $this->rarity;
  }

  /**
   * Set minDamage
   *
   * @param int $minDamage
   * @return \Tower
   */
  public function setMinDamage($minDamage)
  {
    $this->minDamage = $minDamage;
    return $this;
  }

  /**
   * Get minDamage
   *
   * @return int $minDamage
   */
  public function getMinDamage()
  {
    return $this->minDamage;
  }

  /**
   * Set maxDamage
   *
   * @param int $maxDamage
   * @return \Tower
   */
  public function setMaxDamage($maxDamage)
  {
    $this->maxDamage = $maxDamage;
    return $this;
  }

  /**
   * Get maxDamage
   *
   * @return int $maxDamage
   */
  public function getMaxDamage()
  {
    return $this->maxDamage;
  }

  /**
   * Set damageType
   *
   * @param string $damageType
   * @return \Tower
   */
  public function setDamageType($damageType)
  {
    $this->damageType = $damageType;
    return $this;
  }

  /**
   * Get damageType
   *
   * @return string $damageType
   */
  public function getDamageType()
  {
    return $this->damageType;
  }

  /**
   * Set range
   *
   * @param int $range
   * @return \Tower
   */
  public function setRange($range)
  {
    $this->range = $range;
    return $this;
  }

  /**
   * Get range
   *
   * @return int $range
   */
  public function getRange()
  {
    return $this->range;
  }

  /**
   * Set speed
   *
   * @param float $speed
   * @return \Tower
   */
  public function setSpeed($speed)
  {
    $this->speed = $speed;
    return $this;
  }

  /**
   * Get speed
   *
   * @return float $speed
   */
  public function getSpeed()
  {
    return $this->speed;
  }

  /**
   * Set price
   *
   * @param int $price
   * @return \Tower
   */
  public function setPrice($price)
  {
    $this->price = $price;
    return $this;
  }

  /**
   * Get price
   *
   * @return int $price
   */
  public function getPrice()
  {
    return $this->price;
  }

  /**
   * Set upgradePercentage
   *
   * @param int $upgradePercentage
   * @return \Tower
   */
  public function setUpgradePercentage($upgradePercentage)
  {
    $this->upgradePercentage = $upgradePercentage;
    return $this;
  }

  /**
   * Get upgradePercentage
   *
   * @return int $upgradePercentage
   */
  public function getUpgradePercentage()
  {
    return $this->upgradePercentage;
  }

  /**
   * Set sprite
   *
   * @param string $sprite
   * @return \Tower
   */
  public function setSprite($sprite)
  {
    $this->sprite = $sprite;
    return $this;
  }

  /**
   * Get sprite
   *
   * @return string $sprite
   */
  public function getSprite()
  {
    return $this->sprite;
  }

  /**
   * Add animations
   *
   * @param $animations
   */
  public function addAnimation($animations)
  {
    $this->animations[] = $animations;
  }

  /**
   * Remove animations
   *
   * @param <variableType$animations
   */
  public function removeAnimation($animations)
  {
    $this->animations->removeElement($animations);
  }

  /**
   * Get animations
   *
   * @return Doctrine\Common\Collections\Collection $animations
   */
  public function getAnimations()
  {
    return $this->animations;
  }

  /**
   * Add projectiles
   *
   * @param $projectiles
   */
  public function addProjectile($projectiles)
  {
    $this->projectiles[] = $projectiles;
  }

  /**
   * Remove projectiles
   *
   * @param <variableType$projectiles
   */
  public function removeProjectile($projectiles)
  {
    $this->projectiles->removeElement($projectiles);
  }

  /**
   * Get projectiles
   *
   * @return Doctrine\Common\Collections\Collection $projectiles
   */
  public function getProjectiles()
  {
    return $this->projectiles;
  }

  /**
   * Set size
   *
   * @param Fantasytd\TowerBundle\Document\Size $size
   * @return \Tower
   */
  public function setSize(\Fantasytd\TowerBundle\Document\Size $size)
  {
    $this->size = $size;
    return $this;
  }

  /**
   * Get size
   *
   * @return Fantasytd\TowerBundle\Document\Size $size
   */
  public function getSize()
  {
    return $this->size;
  }
}
