<?php

// src/Fantasytd/TowerBundle/Document/Effect.php
namespace Fantasytd\TowerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\EmbeddedDocument
 */
class Effect {

  /**
   * @MongoDB\String
   */
  protected $type;

  /**
   * @MongoDB\int
   */
  protected $time;

  /**
   * @MongoDB\int
   */
  protected $frequency;

  /**
   * @MongoDB\int
   */
  protected $value;

  /**
   * Set type
   *
   * @param string $type
   * @return \Effect
   */
  public function setType($type)
  {
    $this->type = $type;
    return $this;
  }

  /**
   * Get type
   *
   * @return string $type
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set time
   *
   * @param int $time
   * @return \Effect
   */
  public function setTime($time)
  {
    $this->time = $time;
    return $this;
  }

  /**
   * Get time
   *
   * @return int $time
   */
  public function getTime()
  {
    return $this->time;
  }

  /**
   * Set frequency
   *
   * @param int $frequency
   * @return \Effect
   */
  public function setFrequency($frequency)
  {
    $this->frequency = $frequency;
    return $this;
  }

  /**
   * Get frequency
   *
   * @return int $frequency
   */
  public function getFrequency()
  {
    return $this->frequency;
  }

  /**
   * Set value
   *
   * @param int $value
   * @return \Effect
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }

  /**
   * Get value
   *
   * @return int $value
   */
  public function getValue()
  {
    return $this->value;
  }
}
