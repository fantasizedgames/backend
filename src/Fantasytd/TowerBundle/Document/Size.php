<?php

// src/Fantasytd/TowerBundle/Document/Size.php
namespace Fantasytd\TowerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\EmbeddedDocument
 */
class Size {

  /**
   * @MongoDB\int
   */
  protected $x;

  /**
   * @MongoDB\int
   */
  protected $y;

  /**
   * Set x
   *
   * @param int $x
   * @return \Size
   */
  public function setX($x)
  {
    $this->x = $x;
    return $this;
  }

  /**
   * Get x
   *
   * @return int $x
   */
  public function getX()
  {
    return $this->x;
  }

  /**
   * Set y
   *
   * @param int $y
   * @return \Size
   */
  public function setY($y)
  {
    $this->y = $y;
    return $this;
  }

  /**
   * Get y
   *
   * @return int $y
   */
  public function getY()
  {
    return $this->y;
  }
}
