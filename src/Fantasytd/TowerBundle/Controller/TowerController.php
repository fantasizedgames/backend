<?php

namespace Fantasytd\TowerBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Fantasytd\TowerBundle\Document\Tower,
  Fantasytd\TowerBundle\Document\Sprite,
  Fantasytd\TowerBundle\Document\Effect,
  Fantasytd\TowerBundle\Document\Projectile;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class TowerController extends FOSRestController {

  /*
   * @param ParamFetcher $paramFetcher
   */
  public function addTowerAction(ParamFetcher $paramFetcher) {
    // Is this really necessary at this point?
  }

  /**
   * @param ParamFetcher $paramFetcher
   */
  public function removeTowerAction(ParamFetcher $paramFetcher) {
    // Is this really necessary at this point?
  }


  /**
   * @QueryParam(name="name", nullable=true)
   * @QueryParam(name="level", nullable=true)
   * @QueryParam(name="rarity", nullable=true)
   *
   * @QueryParam(name="offset", nullable=true, description="At what point should the list start")
   * @QueryParam(name="amount", nullable=true, description="Amount of games to show")
   * @QueryParam(name="random", nullable=true, description="Pick a random entry from the fetched result")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function listTowersAction(ParamFetcher $paramFetcher) {
    $q = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdTowerBundle:Tower')
      ->createQueryBuilder();

    $name = $paramFetcher->get('name');
    if($name) {
      $q->field('name')->equals($name);
    }

    $level = $paramFetcher->get('level');
    if($level) {
      $q->field('level')->equals($level);
    }

    $rarity = $paramFetcher->get('rarity');
    if($rarity) {
      $q->field('rarity')->equals($rarity);
    }

    // Cut amount according to offset, and amount.
    $offset = $paramFetcher->get('offset');
    if($offset) {
      $q->skip($offset);
    }
    $amount = $paramFetcher->get('amount');
    if($amount) {
      $q->limit($amount);
    }

    $res = $q->getQuery()->execute()->toArray();

    // If set to 1, pick a random entry.
    $random = $paramFetcher->get('random');
    if((bool) $random) {
      $res = array_rand($res);
    }

    $response = new Response();
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->send();
    return $res;
  }

  /**
   * @QueryParam(name="towerId", description="id of the tower")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function loadTowerAction(ParamFetcher $paramFetcher) {
    $response = new Response();
    $tower = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdTowerBundle:Tower')
      ->findById($paramFetcher->get('towerId'));

    if(!count($tower)) {
      $response->setStatusCode(204);
      $response->send();
      return;
    }
    return array_pop($tower);
  }
}
