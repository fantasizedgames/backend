<?php

namespace Fantasytd\TowerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('FantasytdTowerBundle:Default:index.html.twig', array('name' => $name));
    }
}
