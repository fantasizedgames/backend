<?php

namespace Fantasytd\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Fantasytd\UserBundle\Document\Guest;
use Fantasytd\UserBundle\Document\Deck;
use Fantasytd\TowerBundle\Document\Tower;
use Fantasytd\TowerBundle\Document\Animation;
use Fantasytd\TowerBundle\Document\Effect;
use Fantasytd\TowerBundle\Document\Size;
use Fantasytd\TowerBundle\Document\Projectile;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class GuestController extends FOSRestController
{

  /**
   * @RequestParam(name="name", description="Name of the guest")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function addGuestAction( ParamFetcher $paramFetcher )
  {

    $dm = $this->get( 'doctrine_mongodb' )->getManager();
    $guest = new Guest();
    $guest->setName( "[Guest] " . $paramFetcher->get( 'name' ) );
    $guest->setCreated( time() );

    // Pick a deck to the guest.
    $deck = $this->pickGuestDeck();
    $dm->persist( $deck );
    $dm->flush();
    $guest->setDeck( $deck );
    $dm->persist( $guest );
    $dm->flush();

    // Generate access token.
    $guest->setAccessToken( sha1( time() . $guest->getId() ) );
    // Save user in a session.
    $session = new Session();
    $session->start();
    $session->set( $guest->getAccessToken(), $guest );

    $response = new Response();
    $response->setStatusCode( 201 );
    $response->send();
    return $guest;
  }

  /**
   * @QueryParam(name="accesstoken", description="Id of the guest")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function loadGuestAction($id, ParamFetcher $paramFetcher) {
    $response = new Response();
    $guest = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdUserBundle:Guest')
      ->findById($id)
      ->toArray();

    if(!$guest) {
      $response->setStatusCode(204);
      $response->send();
      return;
    }
    $response->send();
    return array_pop($guest);
  }


  /**
   * @RequestParam(name="guestId", description="Id of the guest")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function removeGuestAction(ParamFetcher $paramFetcher) {
    $response = new Response();
    $guest = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdUserBundle:Guest')
      ->createQueryBuilder()
      ->remove()
      ->field('id')->equals($paramFetcher->get('guestId'))
      ->getQuery()->execute();

    $response->send();
    return array_pop($guest);
  }

  public function pickGuestDeck()
  {
    $dm = $this->get( 'doctrine_mongodb' )
      ->getRepository( 'FantasytdTowerBundle:Tower' );

    // Content for deck 1.
    $deckTowers[0]['title'] = 'Fantasy Deck 1';
    $deckTowers[0]['towers'] = array(
      '51375595416da4bb8fd079d1', // Arrow Tower.
      '51375630416da4bb8fd079d2', // Orb Slinger.
      '513756c3416da4bb8fd079d3', // Chaka Raka.
      '5137572f416da4bb8fd079d4', // Laser Turret.
      '5137577c416da4bb8fd079d5', // Fire Snake.
      '513757db416da4bb8fd079d6', // Hydra Pond.
    );
    // Content for deck 2.
    $deckTowers[1]['title'] = 'Fantasy Deck 2';
    $deckTowers[1]['towers'] = array(
      '510e85218c5631443f4049ae', // Cannon Tower.
      '512e641d8b8f9b91dda71094', // Guards Watch.
      '5137257c416da4bb8fd079cc', // Egg Tower.
      '5137260b416da4bb8fd079cd', // Ligtning Spire.
      '513726dd416da4bb8fd079cf', // Fire Pony.
      '513727a0416da4bb8fd079d0', // Scary Plant.
    );

    // Pick one of the decks.
    $deckRoll = rand( 0, 1 );

    // Load towers, and add them to the deck.
    $towers = $this->get( 'doctrine_mongodb' )
      ->getRepository( 'FantasytdTowerBundle:Tower' )
      ->createQueryBuilder()
      ->field( 'id' )->in( array_values( $deckTowers[$deckRoll]['towers'] ) )
      ->getQuery()->execute();

    $deck = new Deck();
    foreach( $towers as $tower )
    {
      $deck->addTower( $tower );
    }
    // Set name of the deck.
    $deck->setTitle( $deckTowers[$deckRoll]['title'] );

    return $deck;
  }
}
