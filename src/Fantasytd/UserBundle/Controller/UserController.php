<?php

namespace Fantasytd\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Fantasytd\UserBundle\Document\User;
use Fantasytd\UserBundle\Document\Statistics;
use Fantasytd\UserBundle\Document\Statistics\CoopStatistics;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;


class UserController extends FOSRestController {

  /**
   * @QueryParam(name="username", nullable=true, description="username of user")
   * @QueryParam(name="sort", nullable=true, description="sort by")
   * @QueryParam(name="order", nullable=true, description="Boolean, 0=desc, 1=asc")
   * @QueryParam(name="offset", nullable=true, description="At what point should the list start")
   * @QueryParam(name="amount", nullable=true, description="Amount of games to show")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function listUsersAction(ParamFetcher $paramFetcher) {
    $users = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdUserBundle:User')
      ->createQueryBuilder()
      ->select('username', 'lastLogin', 'created', 'lastLogin', 'role');

    $username = $paramFetcher->get('username');
    if($username) {
      $users->field('username')->equals($username);
    }

    $order = (bool) $paramFetcher->get('order') ? 'desc' : 'asc';
    $sort = $paramFetcher->get('sort');
    if($sort == 'lastLogin') {
      $users->sort('lastLogin', $order);
    }
    if($sort == 'created') {
      $users->sort('created', $order);
    }

    // Cut amount according to offset, and amount.
    $offset = $paramFetcher->get('offset');
    if($offset) {
      $users->skip($offset);
    }
    $amount = $paramFetcher->get('amount');
    if($amount) {
      $users->limit($amount);
    }

    $res = $users->getQuery()->execute()->toArray();

    $response = new Response();
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->send();
    return $res;
  }

  /**
   * @QueryParam(name="userId", description="Id of the user")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function loadUserAction(ParamFetcher $paramFetcher) {
    $response = new Response();
    $user = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdUserBundle:User')
      ->createQueryBuilder()
      ->select('id', 'username', 'created', 'lastLogin', 'role')
      ->field('id')->equals($paramFetcher->get('userId'))
      ->getQuery()->execute()
      ->toArray();

    if(!$user) {
      $response->setStatusCode(204);
      $response->send();
      return;
    }
    $response->send();
    // Lets remove sensitive information.
    $user = array_pop($user);
    return $user;
  }

  public function alterUserAction(ParamFetcher $paramFetcher) {
    // To come in a later version.
  }

  /**
   * @RequestParam(name="username")
   * @RequestParam(name="email")
   * @RequestParam(name="password")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function addUserAction(ParamFetcher $paramFetcher) {
    $response = new Response();

    $userCheck = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdUserBundle:User')
      ->findByEmail($paramFetcher->get('email'))
      ->toArray();

    // Check if the email is in use.
    if(count($userCheck) >= 1) {
      // The emails is in use, return not acceptable (406).
      $response->setStatusCode(406);
      $response->send();
      return array('error' => 'email is already in use');
    }
    $userCheck = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdUserBundle:User')
      ->findByUsername($paramFetcher->get('username'))
      ->toArray();

    // Check if the email is in use.
    if(count($userCheck) >= 1) {
      // The emails is in use, return not acceptable (406).
      $response->setStatusCode(406);
      $response->send();
      return array('error' => 'username is already in use');
    }

    // Create new user.
    $user = new User();
    $user->setUsername($paramFetcher->get('username'));
    $user->setEmail($paramFetcher->get('email'));
    $user->setCreated(time());
    $user->setRole('user');

    // Lets generate a salt for the password.
    $salt = rand(0, 10000000) . $user->getUsername() . rand(0, 10000000);
    $salt = hash('sha512', $salt);
    $user->setSalt($salt);

    // Hash users password, with our salt.
    $password = hash('sha512', $paramFetcher->get('password') . $salt);
    $user->setPassword($password);

    $dm = $this->get('doctrine_mongodb')->getManager();
    $dm->persist($user);
    $dm->flush();
    // TODO: Login user.
    // Report created (201).
    $response->setStatusCode(201);
    $response->send();
  }

  /**
   * @RequestParam(name="username")
   * @RequestParam(name="password")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function authenticateUserAction(ParamFetcher $paramFetcher) {
    // Find the user, which is about to log in.
    $response = new Response();
    $user = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdUserBundle:User')
      ->findByUsername($paramFetcher->get('username'))
      ->toArray();

    $user = array_pop($user);
    // Has the given password, with target user salt.
    $password = hash('sha512', $paramFetcher->get('password') . $user->getSalt());
    // check the hashed password, agains target password.
    if($password == $user->getPassword()) {
      // User is authenticated, lets generate a access token.
      $accessToken = sha1(time() . $user->getId());
      $_SESSION[$accessToken] = $user;
      $response->setStatusCode(200);
      $response->send();
      return $accessToken;
    }
    // If this is reached, authentication was unsuccessful.
    $response->setStatusCode(401);
    $response->send();
  }
}
