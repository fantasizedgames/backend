<?php

// src/Fantasytd/UserBundle/Document/User.php
namespace Fantasytd\UserBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class User {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\String
   */
  protected $username;

  /**
   * @MongoDB\String
   */
  protected $email;

  /**
   * @MongoDB\String
   */
  protected $password;

  /**
   * @MongoDB\String
   */
  protected $salt;

  /**
   * @MongoDB\int
   */
  protected $created;

  /**
   * @MongoDB\int
   */
  protected $lastLogin;

  /**
   * @MongoDB\EmbedOne(targetDocument="Fantasytd\UserBundle\Document\Deck")
   */
  protected $deck;

  /**
   * @MongoDB\EmbedOne(targetDocument="Fantasytd\UserBundle\Document\Statistics")
   */
  protected $statistics;

  /**
   * @MongoDB\String
   */
  protected $role;

  /**
   * @MongoDB\int
   */
  protected $color;

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set username
   *
   * @param string $username
   * @return \User
   */
  public function setUsername($username)
  {
    $this->username = $username;
    return $this;
  }

  /**
   * Get username
   *
   * @return string $username
   */
  public function getUsername()
  {
    return $this->username;
  }

  /**
   * Set email
   *
   * @param string $email
   * @return \User
   */
  public function setEmail($email)
  {
    $this->email = $email;
    return $this;
  }

  /**
   * Get email
   *
   * @return string $email
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set password
   *
   * @param string $password
   * @return \User
   */
  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }

  /**
   * Get password
   *
   * @return string $password
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * Set created
   *
   * @param int $created
   * @return \User
   */
  public function setCreated($created)
  {
    $this->created = $created;
    return $this;
  }

  /**
   * Get created
   *
   * @return int $created
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * Set lastLogin
   *
   * @param int $lastLogin
   * @return \User
   */
  public function setLastLogin($lastLogin)
  {
    $this->lastLogin = $lastLogin;
    return $this;
  }

  /**
   * Get lastLogin
   *
   * @return int $lastLogin
   */
  public function getLastLogin()
  {
    return $this->lastLogin;
  }

  /**
   * Set deck
   *
   * @param Fantasytd\UserBundle\Document\Deck $deck
   * @return \User
   */
  public function setDeck(\Fantasytd\UserBundle\Document\Deck $deck)
  {
    $this->deck = $deck;
    return $this;
  }

  /**
   * Get deck
   *
   * @return Fantasytd\UserBundle\Document\Deck $deck
   */
  public function getDeck()
  {
    return $this->deck;
  }

  /**
   * Set statistics
   *
   * @param Fantasytd\UserBundle\Document\Statistics $statistics
   * @return \User
   */
  public function setStatistics(\Fantasytd\UserBundle\Document\Statistics $statistics)
  {
    $this->statistics = $statistics;
    return $this;
  }

  /**
   * Get statistics
   *
   * @return Fantasytd\UserBundle\Document\Statistics $statistics
   */
  public function getStatistics()
  {
    return $this->statistics;
  }

  /**
   * Set role
   *
   * @param string $role
   * @return \User
   */
  public function setRole($role)
  {
    $this->role = $role;
    return $this;
  }

  /**
   * Get role
   *
   * @return string $role
   */
  public function getRole()
  {
    return $this->role;
  }

  /**
   * Set salt
   *
   * @param string $salt
   * @return \User
   */
  public function setSalt($salt)
  {
    $this->salt = $salt;
    return $this;
  }

  /**
   * Get salt
   *
   * @return string $salt
   */
  public function getSalt()
  {
    return $this->salt;
  }

  /**
   * Set color
   *
   * @param int $color
   * @return \User
   */
  public function setColor($color)
  {
    $this->color = $color;
    return $this;
  }

  /**
   * Get color
   *
   * @return int $color
   */
  public function getColor()
  {
    return $this->color;
  }
}
