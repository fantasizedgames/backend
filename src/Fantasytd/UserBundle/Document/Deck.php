<?php
// src/Fantasytd/UserBundle/Document/Deck.php
namespace Fantasytd\UserBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/** @MongoDB\EmbeddedDocument */
class Deck {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\String
   */
  protected $title;

  /**
   * @MongoDB\ReferenceMany(targetDocument="Fantasytd\TowerBundle\Document\Tower")
   */
  protected $towers = array();

  /**
   * @MongoDB\ReferenceMany(targetDocument="Fantasytd\CreepBundle\Document\Creep")
   */
  protected $creeps = array();

  public function __construct()
  {
    $this->towers = new \Doctrine\Common\Collections\ArrayCollection();
    $this->creeps = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set title
   *
   * @param string $title
   * @return \Deck
   */
  public function setTitle($title)
  {
    $this->title = $title;
    return $this;
  }

  /**
   * Get title
   *
   * @return string $title
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Add towers
   *
   * @param Fantasytd\TowerBundle\Document\Tower $towers
   */
  public function addTower(\Fantasytd\TowerBundle\Document\Tower $towers)
  {
    $this->towers[] = $towers;
  }

  /**
   * Remove towers
   *
   * @param <variableType$towers
   */
  public function removeTower(\Fantasytd\TowerBundle\Document\Tower $towers)
  {
    $this->towers->removeElement($towers);
  }

  /**
   * Get towers
   *
   * @return Doctrine\Common\Collections\Collection $towers
   */
  public function getTowers()
  {
    return $this->towers;
  }

  /**
   * Add creeps
   *
   * @param Fantasytd\CreepBundle\Document\Creep $creeps
   */
  public function addCreep(\Fantasytd\CreepBundle\Document\Creep $creeps)
  {
    $this->creeps[] = $creeps;
  }

  /**
   * Remove creeps
   *
   * @param <variableType$creeps
   */
  public function removeCreep(\Fantasytd\CreepBundle\Document\Creep $creeps)
  {
    $this->creeps->removeElement($creeps);
  }

  /**
   * Get creeps
   *
   * @return Doctrine\Common\Collections\Collection $creeps
   */
  public function getCreeps()
  {
    return $this->creeps;
  }
}
