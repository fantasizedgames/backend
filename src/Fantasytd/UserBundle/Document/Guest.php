<?php
// src/Fantasytd/UserBundle/Document/Guest.php
namespace Fantasytd\UserBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Guest {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\String
   */
  protected $name;

  /**
   * @MongoDB\int
   */
  protected $accessToken;

  /**
   * @MongoDB\int
   */
  protected $created;

  /**
   * @MongoDB\EmbedOne(targetDocument="Fantasytd\UserBundle\Document\Deck")
   */
  protected $deck;

  /**
   * @MongoDB\int
   */
  protected $color;

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   *
   * @param string $name
   * @return \Guest
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Get name
   *
   * @return string $name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set created
   *
   * @param int $created
   * @return \Guest
   */
  public function setCreated($created)
  {
    $this->created = $created;
    return $this;
  }

  /**
   * Get created
   *
   * @return int $created
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * Set deck
   *
   * @param Fantasytd\UserBundle\Document\Deck $deck
   */
  public function setDeck(\Fantasytd\UserBundle\Document\Deck $deck)
  {
    $this->deck = $deck;
    return $this;
  }

  /**
   * Get deck
   *
   * @return Fantasytd\UserBundle\Document\Deck $deck
   */
  public function getDeck()
  {
    return $this->deck;
  }

  /**
   * Set accessToken
   *
   * @param int $accessToken
   * @return \Guest
   */
  public function setAccessToken($accessToken)
  {
    $this->accessToken = $accessToken;
    return $this;
  }

  /**
   * Get accessToken
   *
   * @return int $accessToken
   */
  public function getAccessToken()
  {
    return $this->accessToken;
  }

  /**
   * Set color
   *
   * @param int $color
   * @return \Guest
   */
  public function setColor($color)
  {
    $this->color = $color;
    return $this;
  }

  /**
   * Get color
   *
   * @return int $color
   */
  public function getColor()
  {
    return $this->color;
  }
}
