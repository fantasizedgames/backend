<?php
// src/Fantasytd/UserBundle/Document/Statistics/CoopStatistics.php
namespace Fantasytd\UserBundle\Document\Statistics;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\EmbeddedDocument
 */
class CoopStatistics {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\int
   */
  protected $games;

  /**
   * @MongoDB\int
   */
  protected $won;

  /**k
   * @MongoDB\int
   */
  protected $lost;

  /**
   * @MongoDB\int
   */
  protected $damageDone;

  /**
   * @MongoDB\int
   */
  protected $creepsKilled;

  /**
   * @MongoDB\int
   */
  protected $towersPlaced;

  /**
   * @MongoDB\int
   */
  protected $lifeLost;

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set games
   *
   * @param int $games
   * @return \CoopStatistics
   */
  public function setGames($games)
  {
    $this->games = $games;
    return $this;
  }

  /**
   * Get games
   *
   * @return int $games
   */
  public function getGames()
  {
    return $this->games;
  }

  /**
   * Set won
   *
   * @param int $won
   * @return \CoopStatistics
   */
  public function setWon($won)
  {
    $this->won = $won;
    return $this;
  }

  /**
   * Get won
   *
   * @return int $won
   */
  public function getWon()
  {
    return $this->won;
  }

  /**
   * Set damageDone
   *
   * @param int $damageDone
   * @return \CoopStatistics
   */
  public function setDamageDone($damageDone)
  {
    $this->damageDone = $damageDone;
    return $this;
  }

  /**
   * Get damageDone
   *
   * @return int $damageDone
   */
  public function getDamageDone()
  {
    return $this->damageDone;
  }

  /**
   * Set creepsKilled
   *
   * @param int $creepsKilled
   * @return \CoopStatistics
   */
  public function setCreepsKilled($creepsKilled)
  {
    $this->creepsKilled = $creepsKilled;
    return $this;
  }

  /**
   * Get creepsKilled
   *
   * @return int $creepsKilled
   */
  public function getCreepsKilled()
  {
    return $this->creepsKilled;
  }

  /**
   * Set towersPlaced
   *
   * @param int $towersPlaced
   * @return \CoopStatistics
   */
  public function setTowersPlaced($towersPlaced)
  {
    $this->towersPlaced = $towersPlaced;
    return $this;
  }

  /**
   * Get towersPlaced
   *
   * @return int $towersPlaced
   */
  public function getTowersPlaced()
  {
    return $this->towersPlaced;
  }

  /**
   * Set lifeLost
   *
   * @param int $lifeLost
   * @return \CoopStatistics
   */
  public function setLifeLost($lifeLost)
  {
    $this->lifeLost = $lifeLost;
    return $this;
  }

  /**
   * Get lifeLost
   *
   * @return int $lifeLost
   */
  public function getLifeLost()
  {
    return $this->lifeLost;
  }
}
