<?php
// src/Fantasytd/UserBundle/Document/Statistics.php
namespace Fantasytd\UserBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\EmbeddedDocument
 */
class Statistics {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\int
   */
  protected $games;

  /**
   * @MongoDB\int
   */
  protected $damageDone;

  /**
   * @MongoDB\int
   */
  protected $lifeLost;

  /**
   * @MongoDB\int
   */
  protected $logins;

  /**
   * @MongoDB\EmbedOne(targetDocument="Fantasytd\UserBundle\Document\Statistics\CoopStatistics")
   */
  protected $coopStatistics;

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set games
   *
   * @param int $games
   * @return \Statistics
   */
  public function setGames($games)
  {
    $this->games = $games;
    return $this;
  }

  /**
   * Get games
   *
   * @return int $games
   */
  public function getGames()
  {
    return $this->games;
  }

  /**
   * Set damageDone
   *
   * @param int $damageDone
   * @return \Statistics
   */
  public function setDamageDone($damageDone)
  {
    $this->damageDone = $damageDone;
    return $this;
  }

  /**
   * Get damageDone
   *
   * @return int $damageDone
   */
  public function getDamageDone()
  {
    return $this->damageDone;
  }

  /**
   * Set lifeLost
   *
   * @param int $lifeLost
   * @return \Statistics
   */
  public function setLifeLost($lifeLost)
  {
    $this->lifeLost = $lifeLost;
    return $this;
  }

  /**
   * Get lifeLost
   *
   * @return int $lifeLost
   */
  public function getLifeLost()
  {
    return $this->lifeLost;
  }

  /**
   * Set logins
   *
   * @param int $logins
   * @return \Statistics
   */
  public function setLogins($logins)
  {
    $this->logins = $logins;
    return $this;
  }

  /**
   * Get logins
   *
   * @return int $logins
   */
  public function getLogins()
  {
    return $this->logins;
  }

  /**
   * Set coopStatistics
   *
   * @param Fantasytd\UserBundle\Document\Statistics\CoopStatistics $coopStatistics
   * @return \Statistics
   */
  public function setCoopStatistics(\Fantasytd\UserBundle\Document\Statistics\CoopStatistics $coopStatistics)
  {
    $this->coopStatistics = $coopStatistics;
    return $this;
  }

  /**
   * Get coopStatistics
   *
   * @return Fantasytd\UserBundle\Document\Statistics\CoopStatistics $coopStatistics
   */
  public function getCoopStatistics()
  {
    return $this->coopStatistics;
  }
}
