<?php

// src/Fantasytd/CreepBundle/Document/Creep.php
namespace Fantasytd\CreepBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document
 */
class Creep {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\String
   */
  protected $name;

  /**
   * @MongoDB\String
   */
  protected $description;

  /**
   * @MongoDB\int
   */
  protected $level;

  /**
   * @MongoDB\int
   */
  protected $rarity;

  /**
   * @MongoDB\int
   */
  protected $damage;

  /**
   * @MongoDB\int
   */
  protected $health;

  /**
   * @MongoDB\int
   */
  protected $speed;

  /**
   * @MongoDB\int
   */
  protected $spread;

  /**
   * @MongoDB\int
   */
  protected $count;

  /**
   * @MongoDB\int
   */
  protected $price;

  /**
   * @MongoDB\int
   */
  protected $goldBounce;

  /**
   * @MongoDB\int
   */
  protected $points;

  /**
   * @MongoDB\String
   */
  protected $armorType;

  /**
   * @MongoDB\boolean
   */
  protected $isBoss;

  /**
   * @MongoDB\String
   */
  protected $type;

  /**
   * @MongoDB\String
   */
  protected $sprite;

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\TowerBundle\Document\Animation"))
   */
  protected $animations;

  /**
   * @MongoDB\EmbedOne(targetDocument="Fantasytd\TowerBundle\Document\Size")
   */
  protected $size;

  public function __construct()
  {
    $this->animations = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   *
   * @param string $name
   * @return \Creep
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * Get name
   *
   * @return string $name
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Set description
   *
   * @param string $description
   * @return \Creep
   */
  public function setDescription($description)
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Get description
   *
   * @return string $description
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * Set level
   *
   * @param int $level
   * @return \Creep
   */
  public function setLevel($level)
  {
    $this->level = $level;
    return $this;
  }

  /**
   * Get level
   *
   * @return int $level
   */
  public function getLevel()
  {
    return $this->level;
  }

  /**
   * Set rarity
   *
   * @param int $rarity
   * @return \Creep
   */
  public function setRarity($rarity)
  {
    $this->rarity = $rarity;
    return $this;
  }

  /**
   * Get rarity
   *
   * @return int $rarity
   */
  public function getRarity()
  {
    return $this->rarity;
  }

  /**
   * Set speed
   *
   * @param int $speed
   * @return \Creep
   */
  public function setSpeed($speed)
  {
    $this->speed = $speed;
    return $this;
  }

  /**
   * Get speed
   *
   * @return int $speed
   */
  public function getSpeed()
  {
    return $this->speed;
  }

  /**
   * Set price
   *
   * @param int $price
   * @return \Creep
   */
  public function setPrice($price)
  {
    $this->price = $price;
    return $this;
  }

  /**
   * Get price
   *
   * @return int $price
   */
  public function getPrice()
  {
    return $this->price;
  }

  /**
   * Set armorType
   *
   * @param string $armorType
   * @return \Creep
   */
  public function setArmorType($armorType)
  {
    $this->armorType = $armorType;
    return $this;
  }

  /**
   * Get armorType
   *
   * @return string $armorType
   */
  public function getArmorType()
  {
    return $this->armorType;
  }

  /**
   * Set isBoss
   *
   * @param boolean $isBoss
   * @return \Creep
   */
  public function setIsBoss($isBoss)
  {
    $this->isBoss = $isBoss;
    return $this;
  }

  /**
   * Get isBoss
   *
   * @return boolean $isBoss
   */
  public function getIsBoss()
  {
    return $this->isBoss;
  }

  /**
   * Set type
   *
   * @param string $type
   * @return \Creep
   */
  public function setType($type)
  {
    $this->type = $type;
    return $this;
  }

  /**
   * Get type
   *
   * @return string $type
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set sprite
   *
   * @param string $sprite
   * @return \Creep
   */
  public function setSprite($sprite)
  {
    $this->sprite = $sprite;
    return $this;
  }

  /**
   * Get sprite
   *
   * @return string $sprite
   */
  public function getSprite()
  {
    return $this->sprite;
  }

  /**
   * Add animations
   *
   * @param $animations
   */
  public function addAnimation($animations)
  {
    $this->animations[] = $animations;
  }

  /**
   * Remove animations
   *
   * @param <variableType$animations
   */
  public function removeAnimation($animations)
  {
    $this->animations->removeElement($animations);
  }

  /**
   * Get animations
   *
   * @return Doctrine\Common\Collections\Collection $animations
   */
  public function getAnimations()
  {
    return $this->animations;
  }

  /**
   * Set size
   *
   * @param $size
   * @return \Creep
   */
  public function setSize($size)
  {
    $this->size = $size;
    return $this;
  }

  /**
   * Get size
   *
   * @return $size
   */
  public function getSize()
  {
    return $this->size;
  }

  /**
   * Set damage
   *
   * @param int $damage
   * @return \Creep
   */
  public function setDamage($damage)
  {
    $this->damage = $damage;
    return $this;
  }

  /**
   * Get damage
   *
   * @return int $damage
   */
  public function getDamage()
  {
    return $this->damage;
  }

  /**
   * Set health
   *
   * @param int $health
   * @return \Creep
   */
  public function setHealth($health)
  {
    $this->health = $health;
    return $this;
  }

  /**
   * Get health
   *
   * @return int $health
   */
  public function getHealth()
  {
    return $this->health;
  }

  /**
   * Set spread
   *
   * @param int $spread
   * @return \Creep
   */
  public function setSpread($spread)
  {
    $this->spread = $spread;
    return $this;
  }

  /**
   * Get spread
   *
   * @return int $spread
   */
  public function getSpread()
  {
    return $this->spread;
  }

  /**
   * Set goldBounce
   *
   * @param int $goldBounce
   * @return \Creep
   */
  public function setGoldBounce($goldBounce)
  {
    $this->goldBounce = $goldBounce;
    return $this;
  }

  /**
   * Get goldBounce
   *
   * @return int $goldBounce
   */
  public function getGoldBounce()
  {
    return $this->goldBounce;
  }

  /**
   * Set count
   *
   * @param int $count
   * @return \Creep
   */
  public function setCount($count)
  {
    $this->count = $count;
    return $this;
  }

  /**
   * Get count
   *
   * @return int $count
   */
  public function getCount()
  {
    return $this->count;
  }

    /**
     * Set points
     *
     * @param int $points
     * @return \Creep
     */
    public function setPoints($points)
    {
        $this->points = $points;
        return $this;
    }

    /**
     * Get points
     *
     * @return int $points
     */
    public function getPoints()
    {
        return $this->points;
    }
}
