<?php

namespace Fantasytd\CreepBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Fantasytd\CreepBundle\Document\Creep,
  Fantasytd\TowerBundle\Document\Sprite;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class CreepController extends FOSRestController {

  /*
   * @param ParamFetcher $paramFetcher
   */
  public function addCreepAction(ParamFetcher $paramFetcher) {
    // Is this really necessary at this point?
  }

  /**
   * @param ParamFetcher $paramFetcher
   */
  public function removeCreepAction(ParamFetcher $paramFetcher) {
    // Is this really necessary at this point?
  }


  /**
   * @QueryParam(name="name", nullable=true)
   * @QueryParam(name="level", nullable=true)
   * @QueryParam(name="rarity", nullable=true)
   * @QueryParam(name="isBoss", nullable=true)
   * @QueryParam(name="type", nullable=true)
   * @QueryParam(name="armorType", nullable=true)
   *
   * @QueryParam(name="offset", nullable=true, description="At what point should the list start")
   * @QueryParam(name="amount", nullable=true, description="Amount of games to show")
   * @QueryParam(name="random", nullable=true, description="Pick a random entry from the fetched result")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function listCreepsAction(ParamFetcher $paramFetcher) {
    $q = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdCreepBundle:Creep')
      ->createQueryBuilder();

    $name = $paramFetcher->get('name');
    if($name) {
      $q->field('name')->equals($name);
    }

    $level = $paramFetcher->get('level');
    if($level) {
      $q->field('level')->equals($level);
    }

    $rarity = $paramFetcher->get('rarity');
    if($rarity) {
      $q->field('rarity')->equals($rarity);
    }

    $isBoss = $paramFetcher->get('isBoss');
    if($isBoss) {
      $q->field('isBoss')->equals($isBoss);
    }

    $type = $paramFetcher->get('type');
    if($type) {
      $q->field('type')->equals($type);
    }

    $armorType = $paramFetcher->get('armorType');
    if($armorType) {
      $q->field('armorType')->equals($armorType);
    }

    // Cut amount according to offset, and amount.
    $offset = $paramFetcher->get('offset');
    if($offset) {
      $q->skip($offset);
    }

    $amount = $paramFetcher->get('amount');
    if($amount) {
      $q->limit($amount);
    }

    $res = $q->getQuery()->execute()->toArray();

    // If set to 1, pick a random entry.
    $random = $paramFetcher->get('random');
    if((bool) $random) {
      $res = array_rand($res);
    }

    $response = new Response();
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->send();
    return $res;
  }

  /**
   * @QueryParam(name="creepId", description="id of the creep")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function loadCreepAction(ParamFetcher $paramFetcher) {
    $response = new Response();
    $creep = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdCreepBundle:Creep')
      ->findById($paramFetcher->get('creepId'));

    if(!count($creep)) {
      $response->setStatusCode(204);
      $response->send();
      return;
    }
    return array_pop($creep);
  }
}
