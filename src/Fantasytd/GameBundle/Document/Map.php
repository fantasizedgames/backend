<?php

// src/Fantasytd/GameBundle/Document/Map.php
namespace Fantasytd\GameBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation\SerializedName;

/**
 * @MongoDB\Document
 */
class Map {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\String
   */
  protected $title;

  /**
   * @MongoDB\String
   */
  protected $objectTitle;

  /**
   * @MongoDB\int
   * @SerializedName("playerLimit")
   */
  protected $playerLimit = array();

  /**
   * @MongoDB\String
   */
  protected $mode;

  /**
   * Set id
   *
   * @param int $id
   * @return \Map
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   *
   * @param string $name
   * @return \Map
   */
  public function setTitle($title)
  {
    $this->title = $title;
    return $this;
  }

  /**
   * Get name
   *
   * @return string $name
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Set playerLimit
   *
   * @param int $playerLimit
   * @return \Map
   */
  public function setPlayerLimit($playerLimit)
  {
    $this->playerLimit = $playerLimit;
    return $this;
  }

  /**
   * Get playerLimit
   *
   * @return int $playerLimit
   */
  public function getPlayerLimit()
  {
    return $this->playerLimit;
  }

  /**
   * Set type
   *
   * @param String $mode
   * @return \Map
   */
  public function setMode($mode)
  {
    $this->mode = $mode;
    return $this;
  }

  /**
   * Get type
   *
   * @return String $mode
   */
  public function getMode()
  {
    return $this->mode;
  }

  /**
   * Set objectTitle
   *
   * @param string $objectTitle
   * @return \Map
   */
  public function setObjectTitle($objectTitle)
  {
    $this->objectTitle = $objectTitle;
    return $this;
  }

  /**
   * Get objectTitle
   *
   * @return string $objectTitle
   */
  public function getObjectTitle()
  {
    return $this->objectTitle;
  }
}
