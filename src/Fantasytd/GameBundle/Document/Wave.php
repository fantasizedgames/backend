<?php

// src/Fantasytd/GameBundle/Document/Wave.php
namespace Fantasytd\GameBundle\Document;

use JMS\Serializer\Annotation\SerializedName;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/** @MongoDB\EmbeddedDocument */
class Wave {

  /**
   * @MongoDB\int
   */
  protected $num;

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\CreepBundle\Document\Creep")
   */
  protected $creeps = array();

  public function __construct()
  {
    $this->creeps = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Set num
   *
   * @param int $num
   * @return \Wave
   */
  public function setNum($num)
  {
    $this->num = $num;
    return $this;
  }

  /**
   * Get num
   *
   * @return int $num
   */
  public function getNum()
  {
    return $this->num;
  }

  /**
   * Add creeps
   *
   * @param Fantasytd\CreepBundle\Document\Creep $creeps
   */
  public function addCreep(\Fantasytd\CreepBundle\Document\Creep $creeps)
  {
    $this->creeps[] = $creeps;
  }

  /**
   * Remove creeps
   *
   * @param <variableType$creeps
   */
  public function removeCreep(\Fantasytd\CreepBundle\Document\Creep $creeps)
  {
    $this->creeps->removeElement($creeps);
  }

  /**
   * Get creeps
   *
   * @return Doctrine\Common\Collections\Collection $creeps
   */
  public function getCreeps()
  {
    return $this->creeps;
  }
}
