<?php

// src/Fantasytd/GameBundle/Document/Result.php
namespace Fantasytd\GameBundle\Document;

use JMS\Serializer\Annotation\SerializedName;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 */
class Result {
  /**
   * @MongoDB\ReferenceOne(
   *   discriminatorMap={
   *     "Fantasytd\UserBundle\Document\User",
   *     "Fantasytd\UserBundle\Document\Guest"
   *   }
   * )
   */
  protected $player;

  /**
   * @MongoDB\String
   * @SerializedName("endState")
   */
  protected $endState;

  /**
   * @MongoDB\int
   */
  protected $score;

  /**
   * @MongoDB\int
   */
  protected $lifes;

  /**
   * @MongoDB\int
   */
  protected $kills;

  /**
   * @MongoDB\int
   * @SerializedName("towersCreated")
   */
  protected $towersCreated;

  /**
   * @MongoDB\int
   * @SerializedName("towersSold")
   */
  protected $towersSold;

  /**
   * @MongoDB\int
   * @SerializedName("goldSpend")
   */
  protected $goldSpend;

  /**
   * @MongoDB\int
   * @SerializedName("goldEarned")
   */
  protected $goldEarned;

  /**
   * @MongoDB\int
   * @SerializedName("damageDone")
   */
  protected $damageDone;

  /**
   * @MongoDB\int
   * @SerializedName("effectsThrown")
   */
  protected $effectsThrown;

  /**
   * Set player
   *
   * @param $player
   * @return \Result
   */
  public function setPlayer($player)
  {
    $this->player = $player;
    return $this;
  }

  /**
   * Get player
   *
   * @return $player
   */
  public function getPlayer()
  {
    return $this->player;
  }

  /**
   * Set score
   *
   * @param int $score
   * @return \Result
   */
  public function setScore($score)
  {
    $this->score = $score;
    return $this;
  }

  /**
   * Get score
   *
   * @return int $score
   */
  public function getScore()
  {
    return $this->score;
  }

  /**
   * Set lifes
   *
   * @param int $lifes
   * @return \Result
   */
  public function setLifes($lifes)
  {
    $this->lifes = $lifes;
    return $this;
  }

  /**
   * Get lifes
   *
   * @return int $lifes
   */
  public function getLifes()
  {
    return $this->lifes;
  }

  /**
   * Set kills
   *
   * @param int $kills
   * @return \Result
   */
  public function setKills($kills)
  {
    $this->kills = $kills;
    return $this;
  }

  /**
   * Get kills
   *
   * @return int $kills
   */
  public function getKills()
  {
    return $this->kills;
  }

  /**
   * Set towersCreated
   *
   * @param int $towersCreated
   * @return \Result
   */
  public function setTowersCreated($towersCreated)
  {
    $this->towersCreated = $towersCreated;
    return $this;
  }

  /**
   * Get towersCreated
   *
   * @return int $towersCreated
   */
  public function getTowersCreated()
  {
    return $this->towersCreated;
  }

  /**
   * Set towersSold
   *
   * @param int $towersSold
   * @return \Result
   */
  public function setTowersSold($towersSold)
  {
    $this->towersSold = $towersSold;
    return $this;
  }

  /**
   * Get towersSold
   *
   * @return int $towersSold
   */
  public function getTowersSold()
  {
    return $this->towersSold;
  }

  /**
   * Set goldSpend
   *
   * @param int $goldSpend
   * @return \Result
   */
  public function setGoldSpend($goldSpend)
  {
    $this->goldSpend = $goldSpend;
    return $this;
  }

  /**
   * Get goldSpend
   *
   * @return int $goldSpend
   */
  public function getGoldSpend()
  {
    return $this->goldSpend;
  }

  /**
   * Set goldEarned
   *
   * @param int $goldEarned
   * @return \Result
   */
  public function setGoldEarned($goldEarned)
  {
    $this->goldEarned = $goldEarned;
    return $this;
  }

  /**
   * Get goldEarned
   *
   * @return int $goldEarned
   */
  public function getGoldEarned()
  {
    return $this->goldEarned;
  }

  /**
   * Set damageDone
   *
   * @param int $damageDone
   * @return \Result
   */
  public function setDamageDone($damageDone)
  {
    $this->damageDone = $damageDone;
    return $this;
  }

  /**
   * Get damageDone
   *
   * @return int $damageDone
   */
  public function getDamageDone()
  {
    return $this->damageDone;
  }

  /**
   * Set effectsThrown
   *
   * @param int $effectsThrown
   * @return \Result
   */
  public function setEffectsThrown($effectsThrown)
  {
    $this->effectsThrown = $effectsThrown;
    return $this;
  }

  /**
   * Get effectsThrown
   *
   * @return int $effectsThrown
   */
  public function getEffectsThrown()
  {
    return $this->effectsThrown;
  }

  /**
   * Set endState
   *
   * @param string $endState
   * @return \Result
   */
  public function setEndState($endState)
  {
    $this->endState = $endState;
    return $this;
  }

  /**
   * Get endState
   *
   * @return string $endState
   */
  public function getEndState()
  {
    return $this->endState;
  }
}
