<?php

// src/Fantasytd/GameBundle/Document/PlayerState.php
namespace Fantasytd\GameBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 * @MongoDB\Document
 */
class PlayerState {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\Boolean
   */
  protected $ready;

  /**
   * @MongoDB\String
   */
  protected $player;

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set ready
   *
   * @param boolean $ready
   * @return \PlayerState
   */
  public function setReady($ready)
  {
    $this->ready = $ready;
    return $this;
  }

  /**
   * Get ready
   *
   * @return boolean $ready
   */
  public function getReady()
  {
    return $this->ready;
  }

  /**
   * Set player
   *
   * @param string $player
   * @return \PlayerState
   */
  public function setPlayer($player)
  {
    $this->player = $player;
    return $this;
  }

  /**
   * Get player
   *
   * @return string $player
   */
  public function getPlayer()
  {
    return $this->player;
  }
}
