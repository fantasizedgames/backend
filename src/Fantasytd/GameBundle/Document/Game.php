<?php

// src/Fantasytd/GameBundle/Document/Game.php
namespace Fantasytd\GameBundle\Document;

use Fantasytd\GameBundle\Document\Map;
use Fantasytd\GameBundle\Document\PlayerState;
use Fantasytd\GameBundle\Document\Wave;
use Fantasytd\UserBundle\Document\User;
use Fantasytd\UserBundle\Document\Guest;
use JMS\Serializer\Annotation\SerializedName;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Game {

  /**
   * @MongoDB\Id
   */
  protected $id;

  /**
   * @MongoDB\String
   */
  protected $title;

  /**
   * @MongoDB\int
   */
  protected $created;

  /**
   * @MongoDB\int
   */
  protected $completed;

  /**
   * @MongoDB\Boolean
   */
  protected $active;

  /**
   * @MongoDB\String
   */
  protected $password;

  /**
   * @MongoDB\ReferenceOne(targetDocument="Fantasytd\GameBundle\Document\Map")
   */
  protected $map;

  /**
   * @MongoDB\String
   */
  protected $mode;

  /**
   * @MongoDB\String
   */
  protected $difficulty;

  /**
   * @MongoDB\int
   */
  protected $gold;

  /**
   * @MongoDB\int
   */
  protected $life;

  /**
   * @MongoDB\int
   * @SerializedName("playerLimit")
   */
  protected $playerLimit;

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\GameBundle\Document\Wave")
   */
  protected $wave = array();

  /**
   * @MongoDB\ReferenceMany(
   *   discriminatorMap={
   *     "Fantasytd\UserBundle\Document\User",
   *     "Fantasytd\UserBundle\Document\Guest"
   *   }
   * )
   */
  protected $players = array();

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\GameBundle\Document\PlayerState" )
   * @SerializedName("playerState")
   */
  protected $playerState = array();

  /**
   * @MongoDB\EmbedMany(targetDocument="Fantasytd\GameBundle\Document\Result" )
   */
  protected $results = array();


  public function __construct()
  {
    $this->players = new \Doctrine\Common\Collections\ArrayCollection();
    $this->playerState = new \Doctrine\Common\Collections\ArrayCollection();
  }

  /**
   * Get id
   *
   * @return id $id
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set title
   *
   * @param string $title
   * @return \Game
   */
  public function setTitle($title)
  {
    $this->title = $title;
    return $this;
  }

  /**
   * Get title
   *
   * @return string $title
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Set created
   *
   * @param int $created
   * @return \Game
   */
  public function setCreated($created)
  {
    $this->created = $created;
    return $this;
  }

  /**
   * Get created
   *
   * @return int $created
   */
  public function getCreated()
  {
    return $this->created;
  }

  /**
   * Set completed
   *
   * @param int $completed
   * @return \Game
   */
  public function setCompleted($completed)
  {
    $this->completed = $completed;
    return $this;
  }

  /**
   * Get completed
   *
   * @return int $completed
   */
  public function getCompleted()
  {
    return $this->completed;
  }

  /**
   * Set active
   *
   * @param boolean $active
   * @return \Game
   */
  public function setActive($active)
  {
    $this->active = $active;
    return $this;
  }

  /**
   * Get active
   *
   * @return boolean $active
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * Set password
   *
   * @param string $password
   * @return \Game
   */
  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }

  /**
   * Get password
   *
   * @return string $password
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * Set map
   *
   * @param Fantasytd\GameBundle\Document\Map $map
   * @return \Game
   */
  public function setMap(\Fantasytd\GameBundle\Document\Map $map)
  {
    $this->map = $map;
    return $this;
  }

  /**
   * Get map
   *
   * @return Fantasytd\GameBundle\Document\Map $map
   */
  public function getMap()
  {
    return $this->map;
  }

  /**
   * Set mode
   *
   * @param string $mode
   * @return \Game
   */
  public function setMode($mode)
  {
    $this->mode = $mode;
    return $this;
  }

  /**
   * Get mode
   *
   * @return string $mode
   */
  public function getMode()
  {
    return $this->mode;
  }

  /**
   * Set difficulty
   *
   * @param string $difficulty
   * @return \Game
   */
  public function setDifficulty($difficulty)
  {
    $this->difficulty = $difficulty;
    return $this;
  }

  /**
   * Get difficulty
   *
   * @return string $difficulty
   */
  public function getDifficulty()
  {
    return $this->difficulty;
  }

  /**
   * Set playerLimit
   *
   * @param int $playerLimit
   * @return \Game
   */
  public function setPlayerLimit($playerLimit)
  {
    $this->playerLimit = $playerLimit;
    return $this;
  }

  /**
   * Get playerLimit
   *
   * @return int $playerLimit
   */
  public function getPlayerLimit()
  {
    return $this->playerLimit;
  }

  /**
   * Add players
   *
   * @param $players
   */
  public function addPlayer($players)
  {
    $this->players[] = $players;
  }

  /**
   * Remove players
   *
   * @param <variableType$players
   */
  public function removePlayer($players)
  {
    $this->players->removeElement($players);
  }

  /**
   * Get players
   *
   * @return Doctrine\Common\Collections\Collection $players
   */
  public function getPlayers()
  {
    return $this->players;
  }

  /**
   * Add playerState
   *
   * @param Fantasytd\GameBundle\Document\PlayerState $playerState
   */
  public function addPlayerState(\Fantasytd\GameBundle\Document\PlayerState $playerState)
  {
    $this->playerState[] = $playerState;
  }

  /**
   * Remove playerState
   *
   * @param <variableType$playerState
   */
  public function removePlayerState(\Fantasytd\GameBundle\Document\PlayerState $playerState)
  {
    $this->playerState->removeElement($playerState);
  }

  /**
   * Get playerState
   *
   * @return Doctrine\Common\Collections\Collection $playerState
   */
  public function getPlayerState()
  {
    return $this->playerState;
  }

  /**
   * Set players
   *
   * @param boolean $players
   * @return \Game
   */
  public function setPlayers($players)
  {
    $this->players = $players;
    return $this;
  }

  /**
   * Add wave
   *
   * @param FantasyTD\GameBundle\Document\Wave $wave
   */
  public function addWave(\FantasyTD\GameBundle\Document\Wave $wave)
  {
    $this->wave[] = $wave;
  }

  /**
   * Remove wave
   *
   * @param <variableType$wave
   */
  public function removeWave(\FantasyTD\GameBundle\Document\Wave $wave)
  {
    $this->wave->removeElement($wave);
  }

  /**
   * Get wave
   *
   * @return Doctrine\Common\Collections\Collection $wave
   */
  public function getWave()
  {
    return $this->wave;
  }

  /**
   * Set gold
   *
   * @param int $gold
   * @return \Game
   */
  public function setGold($gold)
  {
    $this->gold = $gold;
    return $this;
  }

  /**
   * Get gold
   *
   * @return int $gold
   */
  public function getGold()
  {
    return $this->gold;
  }

  /**
   * Set life
   *
   * @param int $life
   * @return \Game
   */
  public function setLife($life)
  {
    $this->life = $life;
    return $this;
  }

  /**
   * Get life
   *
   * @return int $life
   */
  public function getLife()
  {
    return $this->life;
  }

  /**
   * Add results
   *
   * @param Fantasytd\GameBundle\Document\Result $results
   */
  public function addResult(\Fantasytd\GameBundle\Document\Result $results)
  {
    $this->results[] = $results;
  }

  /**
   * Remove results
   *
   * @param <variableType$results
   */
  public function removeResult(\Fantasytd\GameBundle\Document\Result $results)
  {
    $this->results->removeElement($results);
  }

  /**
   * Get results
   *
   * @return Doctrine\Common\Collections\Collection $results
   */
  public function getResults()
  {
    return $this->results;
  }
}
