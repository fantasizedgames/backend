<?php
namespace Fantasytd\GameBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Fantasytd\GameBundle\Document\Map;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;


class MapController extends FOSRestController {

  /**
   * @RequestParam(name="title", description="Map title")
   * @RequestParam(name="playerLimit", description="Maximun amount of players, that can play in this map")
   * @RequestParam(name="mode", description="What game mode is this map created for")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function addMapAction(ParamFetcher $paramFetcher) {
    $map = new Map();

    $map->setTitle($paramFetcher->get('title'));
    $map->setPlayerLimit($paramFetcher->get('playerLimit'));
    $map->setMode($paramFetcher->get('mode'));

    // Save the map.
    $dm = $this->get('doctrine_mongodb')->getManager();
    $dm->persist($map);
    $dm->flush();

    // Tell client, that the map was added.
    $response = new Response();
    $response->setStatusCode(201);
    $response->send();
  }

  /**
   * @QueryParam(name="mode", nullable=true, description="Filter by mode")
   * @QueryParam(name="playerLimit", nullable=true, description="Filter by mode")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function listMapsAction(ParamFetcher $paramFetcher) {
    $q = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdGameBundle:Map')
      ->createQueryBuilder();

    $mode = $paramFetcher->get('mode');
    if(!empty($mode)) {
      $q->field('mode')->equals($mode);
    }

    $playerLimit = $paramFetcher->get('playerLimit');
    if(!empty($playerLimit)) {
      $q->field('playerLimit')->equals( (int) $playerLimit );
    }

    // Build response, and return it.
    $response = new Response();
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->send();

    $maps = $q->getQuery()->execute()->toArray();

    return array_values($maps);
  }

  /**
   * @QueryParam(name="id", nullable=true, description="Search by id")
   * @QueryParam(name="title", nullable=true, description="Search by title")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function getMapAction(ParamFetcher $paramFetcher) {
    $db = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdGameBundle:Map');

    $id = $paramFetcher->get('id');
    $title = $paramFetcher->get('title');
    // Create response for later use.
    $response = new Response();

    if($id) {
      $map = $db->findById($id);
    }else if ($title) {
      $map = $db->findByTitle($title);
    }else {
      // If none selector was provided, return error.
      $response->setStatusCode(403);
      $response->send();
      return null;
    }

    $response->send();
    return $map->toArray();
  }
}
