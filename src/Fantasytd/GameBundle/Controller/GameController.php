<?php

namespace Fantasytd\GameBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Fantasytd\GameBundle\Document\Game;
use Fantasytd\GameBundle\Document\PlayerState;
use Fantasytd\GameBundle\Document\Map;
use Fantasytd\GameBundle\Document\Wave;
use Fantasytd\UserBundle\Document\User;
use Fantasytd\UserBundle\Document\Guest;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;


class GameController extends FOSRestController
{

  /**
   * @QueryParam(name="id", nullable=true, description="id of the games")
   * @QueryParam(name="title", nullable=true, description="title of the games")
   * @QueryParam(name="offset", nullable=true, description="At what point should the list start")
   * @QueryParam(name="amount", nullable=true, description="Amount of games to show")
   * @QueryParam(name="mode", nullable=true, description="Mode of the games")
   * @QueryParam(name="map", nullable=true, description="Map of the games")
   * @QueryParam(name="playerLimit", nullable=true, description="Maximum number of players.")
   * @QueryParam(name="difficulty", nullable=true, description="difficulty of the games.")
   * @RequestParam(name="accesstoken", nullable=false, description="required to recognize guest or user.")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function listGamesAction(ParamFetcher $paramFetcher) {
    $q = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdGameBundle:Game')
      ->createQueryBuilder();

    $id = $paramFetcher->get('id');
    if($id) {
      $q->field('id')->equals($id);
    }

    // Add all search parameters.
    $title = $paramFetcher->get('title');
    if($title) {
      $q->field('title')->equals($title);
    }
    $mode = $paramFetcher->get('mode');
    if($mode) {
      $q->field('mode')->equals($mode);
    }

    $map = $paramFetcher->get('map');
    if($map) {
      $q->field('map.id')->equals($map);
    }
    $playerLimit = $paramFetcher->get('playerLimit');
    if($playerLimit) {
       $q->field('map.playerLimit')->equals($playerLimit);
    }

    $difficulty = $paramFetcher->get('difficulty');
    if($difficulty) {
      $q->field('difficulty')->equals($difficulty);
    }

    // Cut amount according to offset, and amount.
    $offset = $paramFetcher->get('offset');
    if($offset) {
      $q->skip($offset);
    }
    $amount = $paramFetcher->get('amount');
    if($amount) {
      $q->limit($amount);
    }

    $res = $q->getQuery()->execute()->toArray();
    $games = array_values($res);

    $response = new Response();
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->send();

    // If there is only one result, lets pop the outer array.
    if(count($games) == 1) {
      $games = array_pop($games);
    }

    return $games;
  }

  /**
   * @RequestParam(name="title", description="Title of the game")
   * @RequestParam(name="password", nullable=true, description="Password required to enter")
   * @RequestParam(name="map", description="Map that will be played")
   * @RequestParam(name="mode", description="Game mode")
   * @RequestParam(name="difficulty", description="Difficulty of the creeps")
   * @RequestParam(name="playerLimit", description="Amount of players for this game")
   * @RequestParam(name="accesstoken", nullable=false, description="required to recognize guest or user.")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function createGameAction(ParamFetcher $paramFetcher) {
    $response = new Response();
    $game = new Game();
    $game->setTitle($paramFetcher->get('title'));
    $game->setActive(true);
    $game->setPassword($paramFetcher->get('password'));
    $game->setMode($paramFetcher->get('mode'));
    $game->setDifficulty($paramFetcher->get('difficulty'));
    $game->setPlayerLimit($paramFetcher->get('playerLimit'));
    $game->setCreated(time());

    $map = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdGameBundle:Map')
      ->findById($paramFetcher->get('map'));
    $map = $map->toArray();
    $game->setMap(array_pop($map));

    $session = new Session();
    $session->start();

    // Generates waves, and sets player life etc.
    $this->generateWorld( $game );

    $host = $session->get($paramFetcher->get('accesstoken'));

    // Set player state.
    $playerState = new PlayerState();
    $playerState->setPlayer($host->getId());
    $playerState->setReady(false);
    $game->addPlayerState($playerState);

    // If host is a guest, fetch him from the guest Document.
    if($host instanceof Guest) {
      $host = $this->get('doctrine_mongodb')
        ->getRepository('FantasytdUserBundle:Guest')
        ->findById($host->getId())
        ->toArray();
     }else {
      // If not, fetch the user.
      $host = $this->get('doctrine_mongodb')
        ->getRepository('FantasytdUserBundle:User')
        ->findById($paramFetcher->get('host'))
        ->toArray();
    }

    $host = array_pop( $host );
    $this->pickPlayerColor( $host, $game );
    $game->addPlayer( $host );

    $dm = $this->get('doctrine_mongodb')->getManager();
    $dm->persist($game);
    $dm->flush();

    $response->setStatusCode(201);
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->send();
    // Return id to requester.
    return $game->getId();
  }

  private function pickPlayerColor( &$user, $game )
  {
    $exclude = array();
    foreach( $game->getPlayers() as $player )
    {
      // Exclude taken colors.
      $exclude[] = $player->getColor();
    }

    // Keep picking, until a free color is settled for.
    $pickedColor = rand( 0, $game->getPlayerLimit() - 1 );
    while( in_array( $pickedColor, $exclude ) )
    {
      $pickedColor = rand( 0, $game->getPlayerLimit() - 1 );
    }

    $user->setColor( $pickedColor );
  }

  private function generateWorld( &$game )
  {
    /**
     * Define rules for wave generating.
     */
    $difficulty = array_search( strtolower( $game->getDifficulty() ), array( 'graceful', 'elite', 'heroic', 'titan' ));
    $creepStartingLevel = array( 1, 3, 5, 7 );
    $waveCount = array( 20, 20, 30, 40 );
    $bossLevel = array( 20, 10, 10, 10 );
    $levelRaise = array( 15, 10, 10, 10 );
    $difficultyFactor = array( 1, 1.2, 1.5, 2);
    // Player starting points.
    $gold = array( 30, 35, 40, 40 );
    $life = array( 30, 25, 20, 15 );

    // Define points for this game.
    $level = $creepStartingLevel[$difficulty];
    $waveCount = $waveCount[$difficulty];
    $bossLevel = $bossLevel[$difficulty];
    $levelRaise = $levelRaise[$difficulty];
    $difficultyFactor = $difficultyFactor[$difficulty];

    $game->setGold( $gold[$difficulty] );
    $game->setLife( $life[$difficulty] * $game->getPlayerLimit() );

    for( $waveNum = 1; $waveNum <= $waveCount; $waveNum++ )
    {
      $creepQ = $this->get( 'doctrine_mongodb' )
        ->getRepository( 'FantasytdCreepBundle:Creep' )
        ->createQueryBuilder();

      // Get creep with righful level.
      $creepQ->field( 'level' )->equals( $level );

      // Check if we should spawn a boss.
      if( ($waveNum % $bossLevel) == 0 )
      {
        $creepQ->field( 'isBoss' )->equals( true );
      }
      // Make up for MongoDB's laggin random function.
      $count = $creepQ->getQuery()->execute()->count();
      $creepQ->limit( -1 )->skip( rand( 0, $count -1 ) );
      // Fetch the creep.
      $creep = $creepQ->getQuery()->getSingleResult();

      /**
       * Creep stats formulas.
       */
      // Health Pseudo: (CreepLevel * 2) + (DifficultyFactor * (10 + Rarity))
      // / Count + (WaveNumber * DifficultyFactor)
      $health = ($creep->getLevel() * 2) + ($difficultyFactor * (10 + $creep->getRarity())) / $creep->getCount() + ($waveNum * $difficultyFactor);

      // Damage Pseudo: CreepLevel / Count + Rarity.
      $damage = $creep->getLevel() / $creep->getCount() + $creep->getRarity();

      // Gold bounce Pseudo: ((CreepLevel + WaveNumber) / Count)+ Rarity
      $goldBounce = (($creep->getLevel() + $waveNum) / $creep->getRarity());

      $points = (($creep->getLevel() + $waveNum) / $creep->getRarity());

      // If creeps is a boss, add to stats.
      if( $creep->getIsBoss() )
      {
        $health *= 3;
        $damage *= 2;
        $goldBounce *= 3;
        $points *= 2;
      }

      // Apply creep stats, and round to integer.
      $creep->setHealth( round( $health ) );
      $creep->setDamage( round( $damage ) );
      $creep->setGoldBounce( round( $goldBounce ) );
      $creep->setPoints( round( $points ) );

      // Attach creep to wave.
      $wave = new Wave();
      $wave->setNum( $waveNum );
      $wave->addCreep( $creep );
      $game->addWave( $wave );

      // Check if the level should be raised, we raise after the current level.
      if( ($waveNum % $levelRaise) == 0 )
      {
        $level++;
      }
    }
  }

  /**
   * @QueryParam(name="accesstoken", nullable=false, description="Access token of the user")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function loadGameAction( $id, ParamFetcher $paramFetcher )
  {
    $response = new Response();

    // if a game id is not specified, load game user is currently in.
    if( $id == 'undefined' )
    {
      $session = new Session();
      $session->start();
      $player = $session->get( $paramFetcher->get( 'accesstoken' ) );
      $q = $this->get( 'doctrine_mongodb' )
        ->getRepository( 'FantasytdGameBundle:Game' )
        ->createQueryBuilder()
        ->field( 'players.$id' )->equals( new \MongoId( $player->getId() ) );
    }else {
      // Else fetch by stated id.
      $q = $this->get( 'doctrine_mongodb' )
        ->getRepository( 'FantasytdGameBundle:Game' )
        ->createQueryBuilder()
        ->field('id')->equals( $id );
    }
    $res = $q->getQuery()->execute()->toArray();

    $game = array_values( $res );

    if( !count( $game ) ) {
      $response->setStatusCode( 204 );
      $response->send();
      return;
    }
    return array_pop( $game );
  }

  /**
   * @RequestParam(name="id", description="Id of the game.")
   * @RequestParam(name="active", description="alter active state of a game")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function setActiveAction(ParamFetcher $paramFetcher) {
    $response = new Response();

    if($paramFetcher->get('active') == 'true') {
      $active = TRUE;
    }else if($paramFetcher->get('active') == 'false') {
      $active = FALSE;
    }else {
      $response->setStatusCode(403);
      $response->send();
      return;
    }

    $this->get('doctrine_mongodb')->getRepository('FantasytdGameBundle:Game')
      ->createQueryBuilder()
      ->update()
      ->field('active')->set($active)
      ->field('id')->equals($paramFetcher->get('id'))
      ->getQuery()->execute();

    $response->send();
    return;
  }

  /**
   * @RequestParam(name="update", description="What should be updated?")
   * @RequestParam(name="players", description="Players.")
   * @RequestParam(name="password", nullable=true, description="Password for entering the game")
   * @RequestParam(name="accesstoken", nullable=false, description="Access token of user.")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function updateGameAction( $id, ParamFetcher $paramFetcher )
  {
    if($paramFetcher->get('update') == 'join')
    {
      $this->addPlayer( $paramFetcher->get( 'accesstoken' ), $id, $paramFetcher->get( 'password' ) );
    }else {
      $this->setReadyState( $paramFetcher->get( 'accesstoken' ), $id );
    }
  }

  public function setReadyState( $accesstoken, $gameId )
  {
    $dm = $this->get( 'doctrine_mongodb' )->getManager();
    $q = $dm->getRepository( 'FantasytdGameBundle:Game' )
      ->findById( $gameId );
    $game = $q->toArray();
    $game = array_pop( $game );
    // Get the playerStates.
    $playerState = $game->getPlayerState();
    $session = new Session();
    $session->start();
    $player = $session->get( $accesstoken );
    // Traverse all playerStates, for one matching the player.
    foreach( $playerState as $state )
    {
      if( $state->getPlayer() == $player->getId() )
      {
        // First remove current state.
        $game->removePlayerState( $state );
        // Alter and insert it again.
        $state->setReady( true );
        $game->addPlayerState( $state );
        break;
      }
    }
    $game = array( $game->getId() => $game );
    // Save changes.
    $dm->flush();

    $response = new Response();
    $response->setStatusCode( 200 );
    $response->send();
  }

  public function addPlayer( $accesstoken, $gameId, $password=null )
  {
    // Start by fetching the game.
    $response = new Response();
    $dm = $this->get( 'doctrine_mongodb' )->getManager();
    $q = $dm->getRepository( 'FantasytdGameBundle:Game' )
      ->findById( $gameId );
    $game = $q->toArray();
    $game = array_pop( $game );

    // Load the joining player.
    $session = new Session();
    $session->start();
    $player = $session->get( $accesstoken );

    // Check if the game exist.
    if( !$game ) {
      // If no game was found, yield 204 (No content).
      $response->setStatusCode( 204 );
      $response->send();
      return array( 'error' => 103 );
    }

    // Check if player is another game already.
    $isInAGame = $this->get( 'doctrine_mongodb' )
      ->getRepository( 'FantasytdGameBundle:Game' )
      ->createQueryBuilder()
      ->field( 'players.$id' )->equals( new \MongoId( $player->getId() ) )
      ->getQuery()->execute()->count();

    if( $isInAGame )
    {
      $response->setStatusCode( 204 );
      $response->send();
      return array( 'error' => 105 );
    }

    // if the game requires password, check against it.
    if( $game->getPassword() )
    {
      if( $game->getPassword() != $password )
      {
        // return 401 (unauthorized).
        $response->setStatusCode( 401 );
        $response->send();
        return array( 'error' => 106 );
      }
    }

    // Check if there is still room in the game.
    if( $game->getPlayerLimit() <= count( $game->getPlayers() ) )
    {
      $response->setStatusCode( 204 );
      $response->send();
      return array( 'error' => 104 );
    }

    // Checks are done, add the player to the game.
    if( isset( $player ) )
    {
      $playerType = $player instanceof User ? 'User' : 'Guest';

      // Find the user
      $user = $this->get( 'doctrine_mongodb' )
        ->getRepository( 'FantasytdUserBundle:' . $playerType )
        ->findById( $player->getId() )
        ->toArray();
      $user = array_pop( $user );

      $this->pickPlayerColor( $user, $game );

      $game->addPlayer( $user );

      // Add states, for the joining player.
      $state = new PlayerState();
      $state->setReady( false );
      $state->setPlayer( $player->getId() );
      $dm->persist( $state );
      $dm->flush();
      $game->addPlayerState( $state );
      // Save element.
      $dm->flush();

      $response->setStatusCode( 201 );
      $response->send();
    }
  }

  /**
   * @RequestParam(name="gameId", description="Id of the game.")
   * @RequestParam(name="userId", nullable=true,description="Id of the user, who should be added")
   * @RequestParam(name="guestId", nullable=true, description="Id of the guest, who should be added")
   * @RequestParam(name="password", nullable=true, description="Password for entering the game")
   *
   * @param ParamFetcher $paramFetcher
   */
  public function removePlayerAction(ParamFetcher $paramFetcher) {
    // Start by fetching the game.
    $response = new Response();
    $db = $this->get('doctrine_mongodb')
      ->getRepository('FantasytdGameBundle:Game');
    $gameId = $paramFetcher->get('gameId');
    $game = $db->findById($gameId)->toArray();
    $game = array_pop($game);

    // Check if the game exist.
    if(!$game) {
      // If no game was found, yield 204 (No content).
      $response->setStatusCode(204);
      $response->send();
      return;
    }
    // Checks are done, add the player to the game.
    $userId = $paramFetcher->get('userId');
    $guestId = $paramFetcher->get('guestId');
    if(isset($userId)) {
      // Remove player, and save the game.
      $game = $db->findById($gameId)
        ->toArray();
      $game = array_pop($game);
      $players = $game->removePlayer($userId);

      $dm = $this->get('doctrine_mongodb')->getManager();
      $dm->flush();
    } else if($guestId) {
      // Remove player, and save the game.
      $game = $db->findById($gameId)
        ->toArray();
      $game = array_pop($game);
      $players = $game->removeGuest($userId);

      $dm = $this->get('doctrine_mongodb')->getManager();
      $dm->flush();
    }
    $response->send();
  }
}
